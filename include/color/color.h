//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <math/math.h>
#include <math/vector.h>

#include <meta/convert.h>
#include <meta/values/nth.h>
#include <container/plain_tuple.h>

#include <string>
#include <exception>
#include <stdexcept>

//---------------------------------------------------------------------------

namespace asd
{
    namespace color
    {
        template <class T, class Format>
        struct model {};

        struct linear_rgb_format {};
        struct srgb_format {};
        struct argb_format {};
        struct hsv_format {};
        struct hsl_format {};

        template <class T>
        struct model<T, linear_rgb_format>
        {
            constexpr model(T r, T g, T b, T a) noexcept : r(r), g(g), b(b), a(a) {}
            constexpr model(const std::array<T, 4> & a) noexcept : components(a) {}
            constexpr model(const model & m) noexcept : r(m.r), g(m.g), b(m.b), a(m.a) {}

            constexpr model & operator = (const model & m) noexcept {
                r = m.r;
                g = m.g;
                b = m.b;
                a = m.a;

                return *this;
            }

            union
            {
                struct
                {
                    T r, g, b, a;
                };

                math::vector<T> vector;
                std::array<T, 4> components;
            };
        };

        template <>
        struct model<u8, linear_rgb_format>    // linear color can not be represented with 8-bit precision
        {};

        template <class T>
        struct model<T, srgb_format>
        {
            constexpr model(T r, T g, T b, T a) noexcept : r(r), g(g), b(b), a(a) {}
            constexpr model(const std::array<T, 4> & a) noexcept : components(a) {}
            constexpr model(const model & m) noexcept : r(m.r), g(m.g), b(m.b), a(m.a) {}

            constexpr model & operator = (const model & m) noexcept {
                r = m.r;
                g = m.g;
                b = m.b;
                a = m.a;

                return *this;
            }

            union
            {
                struct
                {
                    T r, g, b, a;
                };

                math::vector<T> vector;
                std::array<T, 4> components;
            };
        };

        template <class T>
        struct model<T, argb_format>
        {
            constexpr model(T a, T r, T g, T b) noexcept : a(a), r(r), g(g), b(b) {}
            constexpr model(const std::array<T, 4> & a) noexcept : components(a) {}
            constexpr model(const model & m) noexcept : a(m.a), r(m.r), g(m.g), b(m.b) {}

            constexpr model & operator = (const model & m) noexcept {
                a = m.a;
                r = m.r;
                g = m.g;
                b = m.b;

                return *this;
            }

            union
            {
                struct
                {
                    T a, r, g, b;
                };

                math::vector<T> vector;
                std::array<T, 4> components;
            };
        };

        template <class T>
        struct model<T, hsv_format>
        {
            constexpr model(T h, T s, T v, T a) noexcept : h(h), s(s), v(v), a(a) {}
            constexpr model(const std::array<T, 4> & a) noexcept : components(a) {}
            constexpr model(const model & m) noexcept : h(m.h), s(m.s), v(m.v), a(m.a) {}

            constexpr model & operator = (const model & m) noexcept {
                h = m.h;
                s = m.s;
                v = m.v;
                a = m.a;

                return *this;
            }

            union
            {
                struct
                {
                    T h, s, v, a;
                };

                math::vector<T> vector;
                std::array<T, 4> components;
            };
        };

        template <class T>
        struct model<T, hsl_format>
        {
            constexpr model(T h, T s, T l, T a) noexcept : h(h), s(s), l(l), a(a) {}
            constexpr model(const std::array<T, 4> & a) noexcept : components(a) {}
            constexpr model(const model & m) noexcept : h(m.h), s(m.s), l(m.l), a(m.a) {}

            constexpr model & operator = (const model & m) noexcept {
                h = m.h;
                s = m.s;
                l = m.l;
                a = m.a;

                return *this;
            }

            union
            {
                struct
                {
                    T h, s, l, a;
                };

                math::vector<T> vector;
                std::array<T, 4> components;
            };
        };

        template <class T>
        struct limits
        {};

        template <>
        struct limits<uint8>
        {
            static constexpr int min() {
                return 0;
            }

            static constexpr int max() {
                return 255;
            }
        };

        template <>
        struct limits<uint32>
        {
            static constexpr uint32 min() {
                return 0;
            }

            static constexpr uint32 max() {
                return std::numeric_limits<uint32>::max();
            }
        };

        template <>
        struct limits<float>
        {
            static constexpr float min() {
                return 0.0f;
            }

            static constexpr float max() {
                return 1.0f;
            }
        };

        template <class T, class Format = srgb_format>
        struct value : model<T, Format>
        {
            using limits = color::limits<T>;
            using component_type = T;
            using format_type = Format;
            using model_type = model<T, Format>;

            constexpr value(T x = limits::min(), T y = limits::min(), T z = limits::min(), T alpha = limits::max()) noexcept
                : model_type {x, y, z, alpha} {}

            template <class S>
            explicit constexpr value(const math::vector<T, S> & v) noexcept
                : model_type {v.data} {}

            template <class S>
            explicit constexpr value(const math::vector<T, S> & v, T alpha) noexcept
                : model_type {v[0], v[1], v[2], alpha} {}

            template <class U, useif<!std::is_convertible<const U &, const std::array<T, 4> &>::value && (std::tuple_size<U>::value == 4)>>
            explicit constexpr value(const U & a) noexcept(noexcept(a[0]))
                : model_type {a[0], a[1], a[2], a[3]} {}

            template <class U, useif<!std::is_convertible<const U &, const std::array<T, 3> &>::value && !std::is_convertible<const U &, const std::array<T, 4> &>::value && (std::tuple_size<U>::value >= 3 && std::tuple_size<U>::value <= 4)>>
            explicit constexpr value(const U & a, T alpha) noexcept(noexcept(a[0]))
                : model_type {a[0], a[1], a[2], alpha} {}

            constexpr value(const value & c) noexcept
                : model_type {c} {}

            template <class U>
                requires tag_convertible<U, value>
            explicit constexpr value(const U & v) noexcept(noexcept(convert<value>(v))) : model_type {convert<value>(v)} {}

            constexpr value & operator = (const value & c) noexcept {
                model_type::operator = (c);
                return *this;
            }

            constexpr bool operator == (const value & color) const noexcept {
                return this->components == color.components;
            }

            constexpr T * data() noexcept {
                return this->components.data();
            }

            constexpr const T * data() const noexcept {
                return this->components.data();
            }

            constexpr operator std::array<T, 4> & () noexcept {
                return this->components;
            }

            constexpr operator const std::array<T, 4> & () const noexcept {
                return this->components;
            }

            constexpr T & operator[](size_t idx) {
                return this->components[idx];
            }

            constexpr const T & operator[](size_t idx) const {
                return this->components[idx];
            }
        };

        using linear_rgb = value<float, linear_rgb_format>;
        using rgb = value<float, linear_rgb_format>;
        using srgb = value<float, srgb_format>;
        using srgb8 = value<uint8, srgb_format>;

        using hsv = value<float, hsv_format>;
        using hsl = value<float, hsl_format>;

        static_assert(sizeof(rgb) == 4 * sizeof(float), "Color value is exactly 4 floats");
        static_assert(sizeof(srgb8) == 4 * sizeof(uint8), "Color value is exactly 4 bytes");

        template <class T>
        constexpr T linear_to_srgb(T in) noexcept {
            // return in <= 0.0031308f ? in * 12.92f : 1.055f * math::pow(in, 1.0f / 2.4f) - 0.055f;

            auto v = math::sqrt(in);
            auto out = 0.585122381f * v;

            v = math::sqrt(v);
            out += 0.783140355f * v;

            v = math::sqrt(v);
            return out - 0.368262736f * v;
        }

        template <class T>
        constexpr T srgb_to_linear(T in) noexcept {
            // return in <= 0.04045f ? in / 12.92f : math::pow((in + 0.055f) / 1.055f, 2.4f);

            auto v = +in;
            auto out = 0.012522878f * v;

            v *= in;
            out += 0.682171111f * v;

            v *= in;
            return out + 0.305306011f * v;
        }

        namespace detail
        {
            template <class From, class To>
            struct components_convert
            {};

            template <class T>
            struct components_convert<T, T>
            {
                template <class Format>
                static constexpr auto & convert(const color::value<T, Format> & in) noexcept {
                    return in;
                }
            };

            template <>
            struct components_convert<uint8, float>
            {
                template <class Format>
                static constexpr auto convert(const color::value<uint8, Format> & in) noexcept {
                    return color::value<float, Format>{math::float_vector(in.vector) / 255.0f};
                }
            };

            template <>
            struct components_convert<float, uint8>
            {
                template <class Format>
                static constexpr auto convert(const color::value<float, Format> & in) noexcept {
                    return color::value<uint8, Format>{math::byte_vector(in.vector * 255.0f)};
                }
            };

            template <class From, class To>
            struct format_convert {
                template <class T>
                static constexpr color::value<T, To> convert(const color::value<T, From> & in) noexcept {
                    return format_convert<color::srgb_format, To>::convert(
                        format_convert<From, color::srgb_format>::convert(in));
                }
            };

            template <class Format>
            struct format_convert<Format, Format>
            {
                template <class T>
                static constexpr auto & convert(const color::value<T, Format> & in) {
                    return in;
                }
            };

            template <>
            struct format_convert<color::linear_rgb_format, color::srgb_format>
            {
                template <class T>
                static constexpr color::value<T, color::srgb_format> convert(const color::value<T, color::linear_rgb_format> & in) noexcept {
                    if (std::is_constant_evaluated()) {
                        return { linear_to_srgb(in.r), linear_to_srgb(in.g), linear_to_srgb(in.b), in.a };
                    } else {
                        auto v = linear_to_srgb(in.vector);
                        return { v.x, v.y, v.z, in.a };
                    }
                }
            };

            template <>
            struct format_convert<color::srgb_format, color::linear_rgb_format>
            {
                template <class T>
                static constexpr color::value<T, color::linear_rgb_format> convert(const color::value<T, color::srgb_format> & in) noexcept {
                    if (std::is_constant_evaluated()) {
                        return { srgb_to_linear(in.r), srgb_to_linear(in.g), srgb_to_linear(in.b), in.a };
                    } else {
                        auto v = srgb_to_linear(in.vector);
                        return { v.x, v.y, v.z, in.a };
                    }
                }
            };

            template <>
            struct format_convert<color::srgb_format, color::hsv_format>
            {
                static constexpr color::value<uint8, color::hsv_format> convert(const color::value<uint8, color::srgb_format> & in) noexcept {
                    auto minmax = std::minmax({in.r, in.g, in.b});
                    uint8 delta = minmax.second - minmax.first;

                    if (delta == 0) {
                        return color::value<uint8, color::hsv_format>{0, 0, minmax.second, in.a};
                    }

                    uint8 h = 0;

                    if (in.r >= minmax.second) {
                        h = (in.g - in.b) * 255 / delta;
                    } else if (in.g >=minmax.second) {
                        h = 2 + (in.b - in.r) * 255 / delta;
                    } else {
                        h = 4 + (in.r - in.g) * 255 / delta;
                    }

                    h /= 6;

                    if (h < 0) {
                        h += 255;
                    }

                    return color::value<uint8, color::hsv_format>{h, static_cast<uint8>(delta / minmax.second), minmax.second, in.a};
                }

                static constexpr color::value<float, color::hsv_format> convert(const color::value<float, color::srgb_format> & in) noexcept {
                    auto minmax = std::minmax({in.r, in.g, in.b});
                    float delta = minmax.second - minmax.first;

                    if (delta < 1e-6f || minmax.second <= 0.0f) {
                        return color::value<float, color::hsv_format>{0.0f, 0.0f, minmax.second, in.a};
                    }

                    float h = 0.0f;

                    if (in.r >= minmax.second) {
                        h = (in.g - in.b) / delta;
                    } else if (in.g >= minmax.second) {
                        h = 2.0f + (in.b - in.r) / delta;
                    } else {
                        h = 4.0f + (in.r - in.g) / delta;
                    }

                    h /= 6.0f;

                    if (h < 0.0f) {
                        h += 1.0f;
                    }

                    return color::value<float, color::hsv_format>{h, delta / minmax.second, minmax.second, in.a};
                }
            };

            template <>
            struct format_convert<color::hsv_format, color::srgb_format>
            {
                template <class T>
                static constexpr color::value<T, color::srgb_format> convert(const color::value<T, color::hsv_format> & in) noexcept {
                    using output_type = color::value<T, color::srgb_format>;

                    constexpr T min = color::limits<T>::min();
                    constexpr T max = color::limits<T>::max();

                    if (in.s <= min) {
                        return output_type{in.v, in.v, in.v, in.a};
                    }

                    float hh = in.h;

                    if (hh >= max) {
                        hh = min;
                    }

                    hh = hh * 6.0f / max;

                    int sector = static_cast<int>(hh);
                    T ff = static_cast<T>((hh - sector) * max);

                    switch (sector) {
                        case 0:
                            return output_type{
                                in.v,
                                static_cast<T>(in.v * (max - in.s * (max - ff) / max) / max),
                                static_cast<T>(in.v * (max - in.s) / max),
                                in.a
                            };

                        case 1:
                            return output_type{
                                static_cast<T>(in.v * (max - in.s * ff / max) / max),
                                in.v,
                                static_cast<T>(in.v * (max - in.s) / max),
                                in.a
                            };

                        case 2:
                            return output_type{
                                static_cast<T>(in.v * (max - in.s) / max),
                                in.v,
                                static_cast<T>(in.v * (max - in.s * (max - ff) / max) / max),
                                in.a
                            };

                        case 3:
                            return output_type{
                                static_cast<T>(in.v * (max - in.s) / max),
                                static_cast<T>(in.v * (max - in.s * ff / max) / max),
                                in.v,
                                in.a
                            };

                        case 4:
                            return output_type{
                                static_cast<T>(in.v * (max - in.s * (max - ff) / max) / max),
                                static_cast<T>(in.v * (max - in.s) / max),
                                in.v,
                                in.a
                            };

                        case 5:
                        default:
                            return output_type{
                                in.v,
                                static_cast<T>(in.v * (max - in.s) / max),
                                static_cast<T>(in.v * (max - in.s * ff / max) / max),
                                in.a
                            };
                    }
                }
            };
        }

        template <class T, class Format>
        constexpr const auto & convert(meta::type<color::value<T, Format>>, const color::value<T, Format> & in) noexcept {
            return in;
        }

        template <
            class T, class Format, class InType, class InFormat,
            useif<std::is_same_v<T, InType>>, skipif<std::is_same_v<Format, InFormat>>
        >
        constexpr auto convert(meta::type<color::value<T, Format>>, const color::value<InType, InFormat> & in) noexcept {
            return ::asd::color::detail::format_convert<InFormat, Format>::convert(in);
        }

        template <
            class T, class Format, class InType, class InFormat,
            skipif<std::is_same_v<T, InType>>, useif<std::is_same_v<Format, InFormat>>
        >
        constexpr auto convert(meta::type<color::value<T, Format>>, const color::value<InType, InFormat> & in) noexcept {
            return ::asd::color::detail::components_convert<InType, T>::convert(in);
        }

        template <
            class T, class Format, class InType, class InFormat,
            skipif<std::is_same_v<T, InType>>, skipif<std::is_same_v<Format, InFormat>>
        >
        constexpr auto convert(meta::type<color::value<T, Format>>, const color::value<InType, InFormat> & in) noexcept {
            return ::asd::color::detail::components_convert<InType, T>::convert(::asd::color::detail::format_convert<InFormat, Format>::convert(in));
        }

        template <math::scalar T, class Format>
        constexpr value<T, Format> rotate(const value<T, Format> & c, identity_t<T> amount) {
            color::value<T, color::hsv_format> color(c);
            color.h = math::mod(color.h + amount + limits<T>::max(), limits<T>::max());

            return value<T, Format>{color};
        }

        template <class R, math::scalar T, class Format>
        constexpr R rotate(const value<T, Format> & c, identity_t<T> amount) {
            color::value<T, color::hsv_format> color(c);
            color.h = math::mod(color.h + amount + limits<T>::max(), limits<T>::max());

            return R{color};
        }

        template <class T>
        constexpr void rotate_inplace(value<T, hsv_format> & c, identity_t<T> amount) {
            c.h = math::mod(c.h + amount + limits<T>::max(), limits<T>::max());
        }

        template <class T, class Format>
        constexpr value<T, Format> lighten(const value<T, Format> & c, identity_t<T> amount) {
            color::value<T, color::hsv_format> output(c);
            output.v = math::min(limits<T>::max(), output.v + amount);
            output.s = math::max(limits<T>::min(), output.s - amount);

            return value<T, Format>{output};
        }

        template <class T, class Format>
        constexpr value<T, Format> darken(const value<T, Format> & c, identity_t<T> amount) {
            color::value<T, color::hsv_format> output(c);
            output.v = math::max(limits<T>::min(), output.v - amount);
            output.s = math::min(limits<T>::max(), output.s + amount);

            return value<T, Format>{output};
        }

        template <class T, class Format>
        constexpr value<T, Format> fade(const value<T, Format> & c, identity_t<T> alpha) {
            return value<T, Format>{c[0], c[1], c[2],  math::min(limits<T>::max(), c[3] * alpha)};
        }

        namespace literals
        {
            namespace detail
            {
                template <char... Ch>
                constexpr bool has_hex_int_prefix_v = sizeof...(Ch) > 2 && meta::first_v<char, Ch...> == '0' && (meta::nth_v<1, char, Ch...> == 'x' || meta::nth_v<1, char, Ch...> == 'X');

                template <char... Ch>
                constexpr bool is_decorated_literal_v = has_hex_int_prefix_v<Ch...> || meta::first_v<char, Ch...> == '#';

                template <char Ch>
                constexpr uint8 color_char_to_value() noexcept {
                    static_assert((Ch >= '0' && Ch <= '9') || (Ch >= 'a' && Ch <= 'f') || (Ch >= 'A' && Ch <= 'F'), "incorrect color string format");

                    if constexpr (Ch >= '0' && Ch <= '9') {
                        return static_cast<uint8>(Ch - '0');
                    }

                    if constexpr (Ch >= 'a' && Ch <= 'f') {
                        return static_cast<uint8>(Ch - 'a' + 0xa);
                    }

                    if constexpr (Ch >= 'A' && Ch <= 'F') {
                        return static_cast<uint8>(Ch - 'A' + 0xa);
                    }

                    return 0;
                }

                template <class Color, char... Ch, selectif(0)<sizeof...(Ch) == 3 && !is_decorated_literal_v<Ch...>>>
                constexpr Color str_to_color() noexcept {
                    return {(color_char_to_value<Ch>() / static_cast<float>(0xf))...};
                }

                template <class Color, char... Ch, selectif(1)<sizeof...(Ch) == 6 && !is_decorated_literal_v<Ch...>>>
                constexpr Color str_to_color() noexcept {
                    auto t = make_plain_tuple(color_char_to_value<Ch>()...);

                    return {
                        ((get<0>(t) << 4) + get<1>(t)) / static_cast<float>(0xff),
                        ((get<2>(t) << 4) + get<3>(t)) / static_cast<float>(0xff),
                        ((get<4>(t) << 4) + get<5>(t)) / static_cast<float>(0xff)};
                }

                template <class Color, char... Ch, selectif(2)<sizeof...(Ch) == 4 && !is_decorated_literal_v<Ch...>>>
                constexpr Color str_to_color() noexcept {
                    auto t = make_plain_tuple(color_char_to_value<Ch>()...);

                    return {
                        get<1>(t) / static_cast<float>(0xf),
                        get<2>(t) / static_cast<float>(0xf),
                        get<3>(t) / static_cast<float>(0xf),
                        get<0>(t) / static_cast<float>(0xf),
                    };
                }

                template <class Color, char... Ch, selectif(3)<sizeof...(Ch) == 8 && !is_decorated_literal_v<Ch...>>>
                constexpr Color str_to_color() noexcept {
                    auto t = make_plain_tuple(color_char_to_value<Ch>()...);

                    return {
                        ((get<2>(t) << 4) + get<3>(t)) / static_cast<float>(0xff),
                        ((get<4>(t) << 4) + get<5>(t)) / static_cast<float>(0xff),
                        ((get<6>(t) << 4) + get<7>(t)) / static_cast<float>(0xff),
                        ((get<0>(t) << 4) + get<1>(t)) / static_cast<float>(0xff),
                    };
                }

                template <class Color, char... Ch, skipif<sizeof...(Ch) == 3 || sizeof...(Ch) == 6 || sizeof...(Ch) == 4 || sizeof...(Ch) == 8 || is_decorated_literal_v<Ch...>>>
                constexpr void str_to_color() noexcept {
                    static_assert(sizeof...(Ch) == 3 || sizeof...(Ch) == 6 || sizeof...(Ch) == 4 || sizeof...(Ch) == 8, "incorrect color string size");
                }

                template <class Color, char First, char... Ch, useif<First == '#'>>
                constexpr auto str_to_color() noexcept {
                    return str_to_color<Color, Ch...>();
                }

                template <class Color, char First, char Second, char... Ch, useif<has_hex_int_prefix_v<First, Second, Ch...>>>
                constexpr auto str_to_color() noexcept {
                    return str_to_color<Color, Ch...>();
                }

                constexpr uint8 color_char_to_value(char ch) {
                    if (!((ch >= '0' && ch <= '9') || (ch >= 'a' && ch <= 'f') || (ch >= 'A' && ch <= 'F'))) {
                        BOOST_THROW_EXCEPTION(std::runtime_error("incorrect color string format"));
                    }

                    if (ch >= '0' && ch <= '9') {
                        return static_cast<uint8>(ch - '0');
                    }

                    if (ch >= 'a' && ch <= 'f') {
                        return static_cast<uint8>(ch - 'a' + 0xa);
                    }

                    if (ch >= 'A' && ch <= 'F') {
                        return static_cast<uint8>(ch - 'A' + 0xa);
                    }

                    return 0;
                }

                template <class Color>
                constexpr Color str_to_color(std::string_view s) {
                    if (s[0] == '#') {
                        return str_to_color<Color>(s.substr(1));
                    }

                    if (s[0] == '0' && (s[1] == 'x' || s[1] == 'X')) {
                        return str_to_color<Color>(s.substr(2));
                    }

                    if (s.size() == 3) {
                        return {
                            (color_char_to_value(s[0]) / static_cast<float>(0xf)),
                            (color_char_to_value(s[1]) / static_cast<float>(0xf)),
                            (color_char_to_value(s[2]) / static_cast<float>(0xf)),
                        };
                    }

                    if (s.size() == 4) {
                        return {
                            (color_char_to_value(s[1]) / static_cast<float>(0xf)),
                            (color_char_to_value(s[2]) / static_cast<float>(0xf)),
                            (color_char_to_value(s[3]) / static_cast<float>(0xf)),
                            (color_char_to_value(s[0]) / static_cast<float>(0xf)),
                        };
                    }

                    if (s.size() == 6) {
                        return {
                            (((color_char_to_value(s[0]) << 4) + color_char_to_value(s[1])) / static_cast<float>(0xff)),
                            (((color_char_to_value(s[2]) << 4) + color_char_to_value(s[3])) / static_cast<float>(0xff)),
                            (((color_char_to_value(s[4]) << 4) + color_char_to_value(s[5])) / static_cast<float>(0xff)),
                        };
                    }

                    if (s.size() == 8) {
                        return {
                            (((color_char_to_value(s[2]) << 4) + color_char_to_value(s[3])) / static_cast<float>(0xff)),
                            (((color_char_to_value(s[4]) << 4) + color_char_to_value(s[5])) / static_cast<float>(0xff)),
                            (((color_char_to_value(s[6]) << 4) + color_char_to_value(s[7])) / static_cast<float>(0xff)),
                            (((color_char_to_value(s[0]) << 4) + color_char_to_value(s[1])) / static_cast<float>(0xff)),
                        };
                    }

                    BOOST_THROW_EXCEPTION(std::runtime_error("incorrect color string size"));
                }
            }
        }

        template <class T>
        constexpr T parse(std::string_view s) {
            return color::literals::detail::str_to_color<T>(s);
        }

        template <class T, class Format>
        constexpr color::value<T, Format> convert(meta::type<color::value<T, Format>>, std::string_view s) {
            return color::literals::detail::str_to_color<color::value<T, Format>>(s);
        }

        namespace literals
        {
#if defined(_MSC_VER)
            constexpr auto operator""_rgb(const char * s, std::size_t size) {
                return color::parse<color::linear_rgb>({s, size});
            }

            constexpr auto operator""_hsv(const char * s, std::size_t size) {
                return color::parse<color::hsv>({s, size});
            }

#if _MSC_VER >= 1925
            template <char... Ch>
            constexpr auto operator""_rgb() noexcept {
                return color::literals::detail::str_to_color<color::linear_rgb, Ch...>();
            }

            template <char... Ch>
            constexpr auto operator""_hsv() noexcept {
                return color::literals::detail::str_to_color<color::hsv, Ch...>();
            }
#endif
#else
            constexpr auto operator""_rgb(const char * s, std::size_t size) {
                return color::parse<color::linear_rgb>({s, size});
            }

            constexpr auto operator""_hsv(const char * s, std::size_t size) {
                return color::parse<color::hsv>({s, size});
            }

            template <char... Ch>
            constexpr auto operator""_rgb() noexcept {
                return color::literals::detail::str_to_color<color::linear_rgb, Ch...>();
            }

            template <char... Ch>
            constexpr auto operator""_hsv() noexcept {
                return color::literals::detail::str_to_color<color::hsv, Ch...>();
            }
#endif
        }

        using namespace literals;

        template <class T, class TF, class U, class UF>
        constexpr color::value<T, TF> hue_lerp(const color::value<T, TF> & a, const color::value<U, UF> & b, float t) noexcept {
            color::value<T, color::hsv_format> x(a), y(b);

            if (math::abs(x.h - y.h) <= color::limits<T>::max() / 2) {
                return color::value<T, TF>(color::value<T, color::hsv_format>(math::lerp(x.vector, y.vector, t)));
            }

            if (x.h > y.h) {
                y.h += color::limits<T>::max();
            } else {
                x.h += color::limits<T>::max();
            }

            color::value<T, color::hsv_format> r {math::lerp(x.vector, y.vector, t)};
            r.h = math::mod(r.h, color::limits<T>::max());

            return color::value<T, TF>(r);
        }
    }

    namespace math
    {
        template <class T, class U>
        constexpr color::value<T, color::linear_rgb_format> lerp(const color::value<T, color::linear_rgb_format> & a, const color::value<U, color::linear_rgb_format> & b, float t) noexcept {
            return {math::lerp(a.vector, b.vector, t)};
        }
    }
}
