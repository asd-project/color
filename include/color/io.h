//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <color/color.h>

#if __has_include(<spdlog/fmt/ostr.h>)
    #include <spdlog/fmt/ostr.h>
#endif

//---------------------------------------------------------------------------

namespace asd
{
    namespace color
    {
        template <typename OStream, class T>
        OStream & operator << (OStream & os, const value<T, srgb_format> & color) {
            return os << "srgb(" << color.r << ", " << color.g << ", " << color.b << ", " << color.a << ")";
        }

        template <typename OStream, class T>
        OStream & operator << (OStream & os, const value<T, linear_rgb_format> & color) {
            return os << "rgb(" << color.r << ", " << color.g << ", " << color.b << ", " << color.a << ")";
        }
    }
}
