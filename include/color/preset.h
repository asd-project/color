//---------------------------------------------------------------------------

#pragma once

//---------------------------------------------------------------------------

#include <color/color.h>

//---------------------------------------------------------------------------

namespace asd
{
    namespace color
    {
        constexpr color::rgb transparent = 0x00000000_rgb;           // #00000000

        constexpr color::rgb alice_blue = 0xf0f8ff_rgb;              // #f0f8ff
        constexpr color::rgb antique_white = 0xfaebd7_rgb;           // #faebd7
        constexpr color::rgb aqua = 0x00ffff_rgb;                    // #00ffff
        constexpr color::rgb aquamarine = 0x7fffd4_rgb;              // #7fffd4
        constexpr color::rgb azure = 0xf0ffff_rgb;                   // #f0ffff
        constexpr color::rgb beige = 0xf5f5dc_rgb;                   // #f5f5dc
        constexpr color::rgb bisque = 0xffe4c4_rgb;                  // #ffe4c4
        constexpr color::rgb black = 0x000000_rgb;                   // #000000
        constexpr color::rgb blanched_almond = 0xffebcd_rgb;         // #ffebcd
        constexpr color::rgb blue = 0x0000ff_rgb;                    // #0000ff
        constexpr color::rgb blue_violet = 0x8a2be2_rgb;             // #8a2be2
        constexpr color::rgb brown = 0xa52a2a_rgb;                   // #a52a2a
        constexpr color::rgb burly_wood = 0xdeb887_rgb;              // #deb887
        constexpr color::rgb cadet_blue = 0x5f9ea0_rgb;              // #5f9ea0
        constexpr color::rgb chartreuse = 0x7fff00_rgb;              // #7fff00
        constexpr color::rgb chocolate = 0xd2691e_rgb;               // #d2691e
        constexpr color::rgb coral = 0xff7f50_rgb;                   // #ff7f50
        constexpr color::rgb cornflower_blue = 0x6495ed_rgb;         // #6495ed
        constexpr color::rgb cornsilk = 0xfff8dc_rgb;                // #fff8dc
        constexpr color::rgb crimson = 0xdc143c_rgb;                 // #dc143c
        constexpr color::rgb cyan = 0x00ffff_rgb;                    // #00ffff
        constexpr color::rgb dark_blue = 0x00008b_rgb;               // #00008b
        constexpr color::rgb dark_cyan = 0x008b8b_rgb;               // #008b8b
        constexpr color::rgb dark_golden_rod = 0xb8860b_rgb;         // #b8860b
        constexpr color::rgb dark_gray = 0x434343_rgb;               // #555555 - non html, equal to ISCC-NBS "Davy's gray"
        constexpr color::rgb dark_grey = 0x434343_rgb;               // #555555 - non html, equal to ISCC-NBS "Davy's gray"
        constexpr color::rgb dark_green = 0x006400_rgb;              // #006400
        constexpr color::rgb dark_khaki = 0xbdb76b_rgb;              // #bdb76b
        constexpr color::rgb dark_magenta = 0x8b008b_rgb;            // #8b008b
        constexpr color::rgb dark_olive_green = 0x556b2f_rgb;        // #556b2f
        constexpr color::rgb dark_orange = 0xff8c00_rgb;             // #ff8c00
        constexpr color::rgb dark_orchid = 0x9932cc_rgb;             // #9932cc
        constexpr color::rgb dark_red = 0x8b0000_rgb;                // #8b0000
        constexpr color::rgb dark_salmon = 0xe9967a_rgb;             // #e9967a
        constexpr color::rgb dark_sea_green = 0x8fbc8f_rgb;          // #8fbc8f
        constexpr color::rgb dark_slate_blue = 0x483d8b_rgb;         // #483d8b
        constexpr color::rgb dark_slate_gray = 0x2f4f4f_rgb;         // #2f4f4f
        constexpr color::rgb dark_slate_grey = 0x2f4f4f_rgb;         // #2f4f4f
        constexpr color::rgb dark_turquoise = 0x00ced1_rgb;          // #00ced1
        constexpr color::rgb dark_violet = 0x9400d3_rgb;             // #9400d3
        constexpr color::rgb deep_pink = 0xff1493_rgb;               // #ff1493
        constexpr color::rgb deep_sky_blue = 0x00bfff_rgb;           // #00bfff
        constexpr color::rgb dim_gray = 0x696969_rgb;                // #696969
        constexpr color::rgb dim_grey = 0x696969_rgb;                // #696969
        constexpr color::rgb dodger_blue = 0x1e90ff_rgb;             // #1e90ff
        constexpr color::rgb fire_brick = 0xb22222_rgb;              // #b22222
        constexpr color::rgb floral_white = 0xfffaf0_rgb;            // #fffaf0
        constexpr color::rgb forest_green = 0x228b22_rgb;            // #228b22
        constexpr color::rgb fuchsia = 0xff00ff_rgb;                 // #ff00ff
        constexpr color::rgb gainsboro = 0xdcdcdc_rgb;               // #dcdcdc
        constexpr color::rgb ghost_white = 0xf8f8ff_rgb;             // #f8f8ff
        constexpr color::rgb gold = 0xffd700_rgb;                    // #ffd700
        constexpr color::rgb golden_rod = 0xdaa520_rgb;              // #daa520
        constexpr color::rgb gray = 0x808080_rgb;                    // #808080
        constexpr color::rgb grey = 0x808080_rgb;                    // #808080
        constexpr color::rgb green = 0x008000_rgb;                   // #008000
        constexpr color::rgb green_yellow = 0xadff2f_rgb;            // #adff2f
        constexpr color::rgb honey_dew = 0xf0fff0_rgb;               // #f0fff0
        constexpr color::rgb hot_pink = 0xff69b4_rgb;                // #ff69b4
        constexpr color::rgb indian_red = 0xcd5c5c_rgb;              // #cd5c5c
        constexpr color::rgb indigo = 0x4b0082_rgb;                  // #4b0082
        constexpr color::rgb ivory = 0xfffff0_rgb;                   // #fffff0
        constexpr color::rgb khaki = 0xf0e68c_rgb;                   // #f0e68c
        constexpr color::rgb lavender = 0xe6e6fa_rgb;                // #e6e6fa
        constexpr color::rgb lavender_blush = 0xfff0f5_rgb;          // #fff0f5
        constexpr color::rgb lawn_green = 0x7cfc00_rgb;              // #7cfc00
        constexpr color::rgb lemon_chiffon = 0xfffacd_rgb;           // #fffacd
        constexpr color::rgb light_blue = 0xadd8e6_rgb;              // #add8e6
        constexpr color::rgb light_coral = 0xf08080_rgb;             // #f08080
        constexpr color::rgb light_cyan = 0xe0ffff_rgb;              // #e0ffff
        constexpr color::rgb light_golden_rod_yellow = 0xfafad2_rgb; // #fafad2
        constexpr color::rgb light_gray = 0xd3d3d3_rgb;              // #d3d3d3
        constexpr color::rgb light_grey = 0xd3d3d3_rgb;              // #d3d3d3
        constexpr color::rgb light_green = 0x90ee90_rgb;             // #90ee90
        constexpr color::rgb light_pink = 0xffb6c1_rgb;              // #ffb6c1
        constexpr color::rgb light_salmon = 0xffa07a_rgb;            // #ffa07a
        constexpr color::rgb light_sea_green = 0x20b2aa_rgb;         // #20b2aa
        constexpr color::rgb light_sky_blue = 0x87cefa_rgb;          // #87cefa
        constexpr color::rgb light_slate_gray = 0x778899_rgb;        // #778899
        constexpr color::rgb light_slate_grey = 0x778899_rgb;        // #778899
        constexpr color::rgb light_steel_blue = 0xb0c4de_rgb;        // #b0c4de
        constexpr color::rgb light_yellow = 0xffffe0_rgb;            // #ffffe0
        constexpr color::rgb lime = 0x00ff00_rgb;                    // #00ff00
        constexpr color::rgb lime_green = 0x32cd32_rgb;              // #32cd32
        constexpr color::rgb linen = 0xfaf0e6_rgb;                   // #faf0e6
        constexpr color::rgb magenta = 0xff00ff_rgb;                 // #ff00ff
        constexpr color::rgb maroon = 0x800000_rgb;                  // #800000
        constexpr color::rgb medium_aqua_marine = 0x66cdaa_rgb;      // #66cdaa
        constexpr color::rgb medium_blue = 0x0000cd_rgb;             // #0000cd
        constexpr color::rgb medium_orchid = 0xba55d3_rgb;           // #ba55d3
        constexpr color::rgb medium_purple = 0x9370d8_rgb;           // #9370d8
        constexpr color::rgb medium_sea_green = 0x3cb371_rgb;        // #3cb371
        constexpr color::rgb medium_slate_blue = 0x7b68ee_rgb;       // #7b68ee
        constexpr color::rgb medium_spring_green = 0x00fa9a_rgb;     // #00fa9a
        constexpr color::rgb medium_turquoise = 0x48d1cc_rgb;        // #48d1cc
        constexpr color::rgb medium_violet_red = 0xc71585_rgb;       // #c71585
        constexpr color::rgb midnight_blue = 0x191970_rgb;           // #191970
        constexpr color::rgb mint_cream = 0xf5fffa_rgb;              // #f5fffa
        constexpr color::rgb misty_rose = 0xffe4e1_rgb;              // #ffe4e1
        constexpr color::rgb moccasin = 0xffe4b5_rgb;                // #ffe4b5
        constexpr color::rgb navajo_white = 0xffdead_rgb;            // #ffdead
        constexpr color::rgb navy = 0x000080_rgb;                    // #000080
        constexpr color::rgb old_lace = 0xfdf5e6_rgb;                // #fdf5e6
        constexpr color::rgb olive = 0x808000_rgb;                   // #808000
        constexpr color::rgb olive_drab = 0x6b8e23_rgb;              // #6b8e23
        constexpr color::rgb orange = 0xffa500_rgb;                  // #ffa500
        constexpr color::rgb orange_red = 0xff4500_rgb;              // #ff4500
        constexpr color::rgb orchid = 0xda70d6_rgb;                  // #da70d6
        constexpr color::rgb pale_golden_rod = 0xeee8aa_rgb;         // #eee8aa
        constexpr color::rgb pale_green = 0x98fb98_rgb;              // #98fb98
        constexpr color::rgb pale_turquoise = 0xafeeee_rgb;          // #afeeee
        constexpr color::rgb pale_violet_red = 0xd87093_rgb;         // #d87093
        constexpr color::rgb papaya_whip = 0xffefd5_rgb;             // #ffefd5
        constexpr color::rgb peach_puff = 0xffdab9_rgb;              // #ffdab9
        constexpr color::rgb peru = 0xcd853f_rgb;                    // #cd853f
        constexpr color::rgb pink = 0xffc0cb_rgb;                    // #ffc0cb
        constexpr color::rgb plum = 0xdda0dd_rgb;                    // #dda0dd
        constexpr color::rgb powder_blue = 0xb0e0e6_rgb;             // #b0e0e6
        constexpr color::rgb purple = 0x800080_rgb;                  // #800080
        constexpr color::rgb red = 0xff0000_rgb;                     // #ff0000
        constexpr color::rgb rosy_brown = 0xbc8f8f_rgb;              // #bc8f8f
        constexpr color::rgb royal_blue = 0x4169e1_rgb;              // #4169e1
        constexpr color::rgb saddle_brown = 0x8b4513_rgb;            // #8b4513
        constexpr color::rgb salmon = 0xfa8072_rgb;                  // #fa8072
        constexpr color::rgb sandy_brown = 0xf4a460_rgb;             // #f4a460
        constexpr color::rgb sea_green = 0x2e8b57_rgb;               // #2e8b57
        constexpr color::rgb sea_shell = 0xfff5ee_rgb;               // #fff5ee
        constexpr color::rgb sienna = 0xa0522d_rgb;                  // #a0522d
        constexpr color::rgb silver = 0xc0c0c0_rgb;                  // #c0c0c0
        constexpr color::rgb sky_blue = 0x87ceeb_rgb;                // #87ceeb
        constexpr color::rgb slate_blue = 0x6a5acd_rgb;              // #6a5acd
        constexpr color::rgb slate_gray = 0x708090_rgb;              // #708090
        constexpr color::rgb slate_grey = 0x708090_rgb;              // #708090
        constexpr color::rgb snow = 0xfffafa_rgb;                    // #fffafa
        constexpr color::rgb spring_green = 0x00ff7f_rgb;            // #00ff7f
        constexpr color::rgb steel_blue = 0x4682b4_rgb;              // #4682b4
        constexpr color::rgb tan = 0xd2b48c_rgb;                     // #d2b48c
        constexpr color::rgb teal = 0x008080_rgb;                    // #008080
        constexpr color::rgb thistle = 0xd8bfd8_rgb;                 // #d8bfd8
        constexpr color::rgb tomato = 0xff6347_rgb;                  // #ff6347
        constexpr color::rgb turquoise = 0x40e0d0_rgb;               // #40e0d0
        constexpr color::rgb violet = 0xee82ee_rgb;                  // #ee82ee
        constexpr color::rgb wheat = 0xf5deb3_rgb;                   // #f5deb3
        constexpr color::rgb white = 0xffffff_rgb;                   // #ffffff
        constexpr color::rgb white_smoke = 0xf5f5f5_rgb;             // #f5f5f5
        constexpr color::rgb yellow = 0xffff00_rgb;                  // #ffff00
        constexpr color::rgb yellow_green = 0x9acd32_rgb;            // #9acd32
    }
}
